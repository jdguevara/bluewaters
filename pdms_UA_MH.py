import hoomd
import hoomd.md
import hoomd.deprecated
import numpy as np


def setPairCoeffs():
    # ---=== Pair Potentials ===---

    # Create the initial neighbour list
    nl = hoomd.md.nlist.cell()

    # Initialise a Lennard Jones pair potential
    lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)
    # Set the epislon and sigma values for each atom type
    # that is present in the system first.
    # Values are modified from OPLS-UA/AMBER
    # (note: look into maybe using the hybrid/UA values of F and C)
    pairData = {'CH3': [0.7532, 3.786],
                'SI': [2.4480, 3.385],
                'O': [0.8493, 2.955]}
    # Pairs are determined by geometric average of epsilon
    # and sigma
    for atom1, coeffs1 in pairData.items():
        for atom2, coeffs2 in pairData.items():
            lj.pair_coeff.set(atom1, atom2,
                epsilon=np.sqrt(coeffs1[0] * coeffs2[0]),
                sigma=np.sqrt(coeffs1[1] * coeffs2[1]))


def setBondCoeffs():
    # ---=== Bond Potentials ===---
    # Initialise a harmonic bond
    harmonicBond = hoomd.md.bond.harmonic()
    # Set the k and r0 values for each bond
    # Values are from hybrid/UA force field of Frischknecht and Curro
    harmonicBond.bond_coeff.set('SI-O', k=700.24, r0=3.28)
    harmonicBond.bond_coeff.set('SI-CH3', k=379.3, r0=3.80)
    # Do I still have to take the others as bonds? (i.e. O-O since they're not bonded)
    # A: It would seem, according to Frischknecht and Curro that indeed we'd only worry about Si-O and Si-CH3 bonds

def setAngleCoeffs():
    # ---=== Angle Potentials ===---
    # Initialise a harmonic angle
    harmonicAngle = hoomd.md.angle.harmonic()
    # Set the k and t0 values for each angle
    # Values are modified from hybrid/UA force field of Frischknecht and Curro
    harmonicAngle.angle_coeff.set('SI-O-SI', k=46.42, t0=5.11)
    harmonicAngle.angle_coeff.set('O-SI-O', k=333.92, t0=3.76) 
    harmonicAngle.angle_coeff.set('CH3-SI-CH3', k=164.05, t0=3.81) 
    harmonicAngle.angle_coeff.set('O-SI-CH3', k=164.05, t0=3.86) 
    # k values were modified from kcal/(mol?)deg**2 to Cal/rad**2 to fit the HOOMD format

def multiHarmonicTorsion(theta, V0, V1, V2, V3, V4):
    # Definition of multiharmonic dihedral equation based on 5 input parameters to be used by HOOMD
    # The equation can be written as: V = \sum_{i = 0}^{4} V_{i} cos^{i}(\theta)
    V = V0 + V1 * np.cos(theta) + V2 * ((np.cos(theta))**2) + V3 * ((np.cos(theta))**3) + V4 * ((np.cos(theta))**4)
    F = V1 * np.sin(theta) + 2 * V2 * np.cos(theta) * np.sin(theta) + 3 * V3 * ((np.cos(theta))**2) * np.sin(theta) + 4 * V4 * ((np.cos(theta))**3) * np.sin(theta)
    return (V, F)


def setDihedralCoeffs():
    # ---=== Dihedral Potentials ===---
    # Initialise a tabulated dihedral
    harmonicDihedral = hoomd.md.dihedral.table(width=1000)
    # Set the k and r0 values for each bond
    # Values are modified from OPLS-AA according to Bhatta
    harmonicDihedral.dihedral_coeff.set('S-CA-CA-S', func=multiHarmonicTorsion, coeff=dict(V0=11.8132, V1=0.6284, V2=-16.9304, V3=1.5916, V4=7.542))
    harmonicDihedral.dihedral_coeff.set('CA-CA-CA-S', func=multiHarmonicTorsion, coeff=dict(V0=11.8132, V1=-0.6284, V2=-16.9304, V3=-1.5916, V4=7.542))
    harmonicDihedral.dihedral_coeff.set('CA-CA-S-CA', func=multiHarmonicTorsion, coeff=dict(V0=0.0, V1=0.0, V2=0.0, V3=0.0, V4=0.0))
    harmonicDihedral.dihedral_coeff.set('CA-CA-CT-CT', func=multiHarmonicTorsion, coeff=dict(V0=1.27, V1=4.508, V2=56.572, V3=-89.188, V4=26.8752))
    harmonicDihedral.dihedral_coeff.set('CA-CT-CT-CT', func=multiHarmonicTorsion, coeff=dict(V0=9.7876, V1=-25.5784, V2=42.988, V3=122.78, V4=44.556))
    harmonicDihedral.dihedral_coeff.set('CA-CA-CA-CA', func=multiHarmonicTorsion, coeff=dict(V0=0.0, V1=0.0, V2=0.0, V3=0.0, V4=0.0))
    harmonicDihedral.dihedral_coeff.set('CT-CA-CA-S', func=multiHarmonicTorsion, coeff=dict(V0=0.0, V1=0.0, V2=0.0, V3=0.0, V4=0.0))
    harmonicDihedral.dihedral_coeff.set('CA-CA-CA-CT', func=multiHarmonicTorsion, coeff=dict(V0=0.0, V1=0.0, V2=0.0, V3=0.0, V4=0.0))
    harmonicDihedral.dihedral_coeff.set('CT-CT-CT-CT', func=multiHarmonicTorsion, coeff=dict(V0=7.5688, V1=-13.9616, V2=5.866, V3=28.5672, V4=1.1436))


if __name__ == "__main__":
    # Start with the name of the file to be run
    fileName = './p3ht.xml'

    # Initialise the system
    hoomd.context.initialize("")
    system = hoomd.deprecated.init.read_xml(filename = fileName)

    # Set the coefficients
    setPairCoeffs()
    setBondCoeffs()
    setAngleCoeffs()
    setDihedralCoeffs()

    # Set the groups
    all = hoomd.group.all()

    # Create the integrator
    hoomd.md.integrate.mode_standard(dt=0.001)
    integrator = hoomd.md.integrate.nvt(group=all, tau=1.0, kT=1.0)

    # Set the total runtime
    runTime = 1e5

    # Set the output files
    hoomd.dump.gsd(group=all, filename=fileName.replace("xml", "gsd"), period=int(runTime/100), overwrite=True)
    hoomd.run(runTime)
