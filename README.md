# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for storing all pertinent python, mbuild, and hoomd code needed for the simulation of PDMS

### How do I get set up? ###

All python notebooks (.ipynb) files can be seen and compiled with: 
	$ jupyter notebook <filename> ...
or for the whole repo: 
	$ jupyter notebook
python files can be compiled normally using: 
	$ python <filename>
.hoomdxml can be seen with: 
	vmd -hoomd <filename>

### Contribution guidelines ###

No real guidelines at this point in time, so long as all files are not messed with irreversably - July 29, 2017

### Who do I talk to? ###

Repo owner/admin: Jaime Guevara